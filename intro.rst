Basic usage
===========

`Information for developpers`

API information
---------------

This API is build with framework : ``django-rest-framework``.
It is available in ``http`` protocole for now.
For example, if you send ``GET`` request on `/restapi/beer/` index,
you obtain the endpoint list of module beer.

.. code-block:: bash

   $ curl -XGET http://51.68.230.193/restapi/beer/

.. code-block:: json

   {"beer-type": "http://51.68.230.193/restapi/beer/beer-type/", "beer": "http://51.68.230.193/restapi/beer/beer/", "bar": "http://51.68.230.193/restapi/beer/bar/", "brewery": "http://51.68.230.193/restapi/beer/brewery/", "beer-list": "http://51.68.230.193/restapi/beer/beer-list/", "beer-item": "http://51.68.230.193/restapi/beer/beer-item/", "beer-picture": "http://51.68.230.193/restapi/beer/beer-picture/"}


.. note:: For improve the example design of this documentation, you
	  cand add the command ``python -m json.tool`` of output 
          of curl command. And you get a indent return of JSON. 


.. code-block:: bash

   $ curl -s -XGET https://51.68.230.193/restapi/beer/beer/ | python -m json.tool

.. code-block:: json

   {
       "beer-type": "http://51.68.230.193/restapi/beer/beer-type/",
       "beer": "http://51.68.230.193/restapi/beer/beer/",
       "bar": "http://51.68.230.193/restapi/beer/bar/",
       "brewery": "http://51.68.230.193/restapi/beer/brewery/",
       "beer-list": "http://51.68.230.193/restapi/beer/beer-list/",
       "beer-item": "http://51.68.230.193/restapi/beer/beer-item/",
       "beer-picture": "http://51.68.230.193/restapi/beer/beer-picture/"
   }


Endpoints description
---------------------

It is possible to obtain a description of each endpoint with
``OPTIONS`` method.

.. code-block:: bash

   $ curl -XOPTIONS http://51.68.230.193/restapi/beer/beer-item/

.. code-block:: json

   {
       "name": "Beer Item",
       "description": "",
       "renders": [
           "application/json",
           "text/html"
       ],
       "parses": [
           "application/json",
           "application/x-www-form-urlencoded",
           "multipart/form-data"
       ]
   }

This description contain the endpoint name and a description if it neccessary.
The ``renders`` key give mime type. You can change the header with ``Accept``
in your request if the default type is not appropriate. The most of time,
the response type are ``application/json`` for default API call and ``text/html`` 
for the browser calls : http://51.68.230.193/restapi/beer/beer-item/


Authentication
--------------

Every endpoint needs authentication. You should authenticate for obtain an 
acces token, who is available one hour.

.. code-block:: bash

   $ curl -XOPTIONS http://51.68.230.193/restapi/beer/beer-item/
   HTTP/1.1 401 Unauthorized
   Content-Type: application/json
   Allow: GET, POST, HEAD, OPTIONS
   WWW-Authenticate: Token

.. code-block:: json

   {
       "detail": "Informations d'authentification non fournies."
   }

.. code-block:: bash

   $ client_id='DqUdH3KDydzh9A85UDSXkcd3XQdlTG4qFNLJ1kyr'
   $ client_secret='fedcba9876543210deadbeef0123456789abcdef' (example)
   $ username='toto@gmail.com' (example)
   $ password='password' (example)
   $ curl -X POST -d 'grant_type=password&username=$username&password=$password' -u "$client_id:$client_secret" -XOPTIONS http://51.68.230.193/authenticate/token/
   HTTP/1.1 200 OK
   Content-Type: application/json
   Allow: GET, POST, HEAD, OPTIONS

.. code-block:: json

   {
      "access_token": "ATiM10L0LNaldJPk12drXCjbhoeDR8",
      "expires_in": 36000,
      "refresh_token": "II4UBhXhpVDEKWmsUQxDzkj3OMjW1p",
      "scope": "read groups write",
      "token_type": "Bearer"
   }

You should retrieve the value of ``access_token`` for the next calls.

Récupérer des données
---------------------

.. code-block:: bash

   $ token='ATiM10L0LNaldJPk12drXCjbhoeDR8'
   $ curl -H "Authorization: Bearer $token" http://51.68.230.193/restapi/beer/beer-type/
   HTTP/1.1 200 OK
   Content-Type: application/json
   Allow: GET, POST, HEAD, OPTIONS

.. code-block:: json

   {
      "count": 5,
      "next": null,
      "previous": null,
      "results": [
          {
              "id": 5,
              "name": "Rousse"
          },
          {
              "id": 4,
              "name": "Brune"
          },
          {
              "id": 3,
              "name": "Blonde"
          },
          {
              "id": 2,
              "name": "Blanche"
          },
          {
              "id": 1,
              "name": "IPA"
          }
      ]
   }
