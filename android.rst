Android usage
=============

`Information for developpers`

Project information
-------------------

The android project is build with : ``Retrofit``.
It is the most simple dependance android to access
database. We will see the different method on few endpoints.
if you doesn't read the basic usage page, got to previous page.

Methods
#######
- **GET**
- **POST**
- **PUT**
- **DELETE**


Authentication
--------------

This is an example of authentication on ``Django-rest-framework`` API
with ``Retrofit``.

.. code-block:: java

   @Headers({
       "Content-Type': 'application/x-www-form-urlencoded",
       "Accept: application/json",
   })
   @POST("authenticate/token/")
   @FormUrlEncoded
   Call<AccessToken> getAccessToken(
       @Field("grant_type") String grantType,
       @Field("password") String password,
       @Field("username") String username,
       @Field("client_id") String clientId,
       @Field("client_secret") String clientrSecret
   );
   
The response of this call is on the previous previous page.
We give few example of request with different method.


GET
---

.. code-block:: java

   @GET("restapi/user/member/")
   Call<UserDataContainer> getMemberByEmail(
       @Header("Authorization") String token,
       @Query("email") String email
   );

You should pass the acces token in API call every time.


POST
----

.. code-block:: java

   @FormUrlEncoded
   @POST("/restapi/beer/beer-list/")
   Call<ApiResponse> addBeerWish(
       @Header("Authorization") String token,
       @Field("beer") int beer,
       @Field("member") int member
   );


Function call
-------------

When you call this function, you should create a callback like that :

.. code-block:: java

   Call<AccessToken> accessTokenCall = userService.getAccessToken(
       "password", password, email, client_id, client_secret
   );

   accessTokenCall.enqueue(new Callback<AccessToken>() {
       @Override
       public void onResponse(Call<AccessToken> call, Response<AccessToken> response) {
           if(response.code() == 200){
               final String accessToken = "Bearer " + response.body().getAccessToken();
               // Behavior if authenticate succeed
           }else{
               // Behavior if authenticate fail
           }
       }
       @Override
       public void onFailure(Call<AccessToken> call, Throwable t) {
           // Behavior the service is unavailable
	   Toast.makeText(
               MaltiApplication.getInstance(),
               "Problème d'accès a la base de données", Toast.LENGTH_LONG
           ).show();
       }
   });


