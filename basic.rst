Basic usage
===========

`Information for developpers`

Database information
--------------------

The different database tables are : ``BeerType``, ``Beer``,
``Bar``, ``Brewery``, ``BeerList``, ``BeerPicture``, ``BeerItem``,
``StaffMember``, ``Member``, ``FriendList``, ``Location``, ``Post``,
``Comment``, ``Like``

BeerType
####
- **id :** integer
- **name :** string

Beer
####
- **id :** integer
- **beer_type :** integer (FK)
- **member :** integer (FK)
- **brewery :** integer (FK)
- **name :** string (50)
- **review :** float
- **content :** string (5000)
- **description :** string (5000)
- **short_description :** string (250)
- **alcohol_level :** float
- **smell :** string (255)
- **aspect :** string (255)
- **taste :** string (255)
- **picture :** string
- **price :** float
- **litter :** float
- **posted_on :** date
- **moderated_on :** date
- **state :** integer

Brewery
#######
- **id :** integer
- **member :** integer (FK)
- **location :** integer (FK)
- **name :** string (50)
- **description :** string (5000)
- **phone :** string (15)
- **website :** string (255)
- **picture :** string
- **posted_on :** date
- **state :** integer

Bar
###
- **id :** integer
- **member :** integer (FK)
- **location :** integer (FK)
- **name :** string (50)
- **description :** string (5000)
- **picture :** string
- **outside :** boolean
- **posted_on :** date
- **state :** integer

BeerList
########
- **id :** integer
- **member :** integer (FK)
- **beer :** integer (FK)
- **drinked :** boolean
- **posted_on :** date

BeerItem
########
- **id :** integer
- **member :** integer (FK)
- **beer :** integer (FK)
- **bar :** integer (FK)
- **price :** float
- **is_draft_beer :** boolean
- **posted_on :** date
- **moderated_on :** date
- **state :** integer

BeerItem
########
- **id :** integer
- **member :** integer (FK)
- **beer :** integer (FK)
- **picture :** string
- **picture_url :** string
- **posted_on :** date

Location
########
- **id :** integer
- **address :** string (255)
- **city :** string (5)
- **postal_code :** string (5)
- **country :** string (50)
- **longitude :** float
- **latitude :** float

Post
####
- **id :** integer
- **member :** integer (FK)
- **beer :** integer (FK)
- **review :** float
- **content :** string (5000)
- **posted_on :** date
- **moderated_on :** date
- **state :** integer

Comment
#######
- **id :** integer
- **member :** integer (FK)
- **post :** integer (FK)
- **content :** string (5000)
- **posted_on :** date

Like
####
- **id :** integer
- **member :** integer (FK)
- **post :** integer (FK)
- **comment :** integer (FK)
- **posted_on :** date

Member
######
- **id :** integer
- **email :** string (255)
- **first_name :** string (50)
- **last_name :** string (50)
- **is_super_user** boolean
- **location :** integer (FK)
- **password :** string (128)
- **last_login :** date
- **description :** string (5000)
- **civility :** integer
- **phone :** string (15)
- **picture_url :** string (255)
- **has_social_connect :** integer
- **state :** integer

FriendList
##########
- **id :** integer
- **ask_member** integer (FK)
- **asked_member** integer (FK)
- **ask_date** date
- **valid_date** date

Member
######
**Proxy Model of Member, with is_super_user as True**

FriendListName (sql view)
##############
- **id** integer
- **ask_username** string(50)
- **asked_username** string(50)
- **ask_date** date
- **valid_date** date

MemberStats (sql view)
###########
- **id** integer
- **username** string(50)
- **drinked_beer** integer
- **added_brewery** integer
- **added_beer** integer
- **wished_beer** integer
- **friend** integer


API information
---------------

This API is build with framework : ``django-rest-framework``.
It is available in ``http`` protocol for now.
For example, if you send ``GET`` request on `/restapi/beer/` index,
you will obtain the endpoint list of beer module.

.. code-block:: bash

   $ curl -XGET http://51.68.230.193/restapi/beer/

.. code-block:: json

   {"beer-type": "http://51.68.230.193/restapi/beer/beer-type/", "beer": "http://51.68.230.193/restapi/beer/beer/", "bar": "http://51.68.230.193/restapi/beer/bar/", "brewery": "http://51.68.230.193/restapi/beer/brewery/", "beer-list": "http://51.68.230.193/restapi/beer/beer-list/", "beer-item": "http://51.68.230.193/restapi/beer/beer-item/", "beer-picture": "http://51.68.230.193/restapi/beer/beer-picture/"}


.. note:: To improve the example design of this documentation, you
	      can add the command ``python -m json.tool`` of output 
          of curl command. And you get an indent return of JSON. 


.. code-block:: bash

   $ curl -s -XGET https://51.68.230.193/restapi/beer/beer/ | python -m json.tool

.. code-block:: json

   {
       "beer-type": "http://51.68.230.193/restapi/beer/beer-type/",
       "beer": "http://51.68.230.193/restapi/beer/beer/",
       "bar": "http://51.68.230.193/restapi/beer/bar/",
       "brewery": "http://51.68.230.193/restapi/beer/brewery/",
       "beer-list": "http://51.68.230.193/restapi/beer/beer-list/",
       "beer-item": "http://51.68.230.193/restapi/beer/beer-item/",
       "beer-picture": "http://51.68.230.193/restapi/beer/beer-picture/"
   }


Endpoints description
---------------------

It is possible to obtain a description of each endpoint with
``OPTIONS`` method.

.. code-block:: bash

   $ curl -XOPTIONS http://51.68.230.193/restapi/beer/beer-item/

.. code-block:: json

   {
       "name": "Beer Item",
       "description": "",
       "renders": [
           "application/json",
           "text/html"
       ],
       "parses": [
           "application/json",
           "application/x-www-form-urlencoded",
           "multipart/form-data"
       ]
   }

This description contains the endpoint name and a description if it neccessary.
The ``renders`` key give mime type. You can change the header with ``Accept``
in your request if the default type is not appropriate. The most of time,
the response type are ``application/json`` for default API call and ``text/html`` 
for the browser calls : http://51.68.230.193/restapi/beer/beer-item/


Authentication
--------------

Every endpoint needs authentication. You should authenticate for obtain an 
acces token, who is available one hour.

.. code-block:: bash

   $ curl -XOPTIONS http://51.68.230.193/restapi/beer/beer-item/
   HTTP/1.1 401 Unauthorized
   Content-Type: application/json
   Allow: GET, POST, HEAD, OPTIONS
   WWW-Authenticate: Token

.. code-block:: json

   {
       "detail": "Informations d'authentification non fournies."
   }

.. code-block:: bash

   $ client_id='DqUdH3KDydzh9A85UDSXkcd3XQdlTG4qFNLJ1kyr'
   $ client_secret='fedcba9876543210deadbeef0123456789abcdef' (exemple)
   $ username='toto@gmail.com' (example)
   $ password='password' (example)
   $ curl -X POST -d 'grant_type=password&username=$username&password=$password' -u "$client_id:$client_secret" -XOPTIONS http://51.68.230.193/authenticate/token/
   HTTP/1.1 200 OK
   Content-Type: application/json
   Allow: GET, POST, HEAD, OPTIONS

.. code-block:: json

   {
      "access_token": "ATiM10L0LNaldJPk12drXCjbhoeDR8",
      "expires_in": 36000,
      "refresh_token": "II4UBhXhpVDEKWmsUQxDzkj3OMjW1p",
      "scope": "read groups write",
      "token_type": "Bearer"
   }

You should retrieve the value of ``access_token`` for the next calls.

Get data
--------

.. code-block:: bash

   $ token='ATiM10L0LNaldJPk12drXCjbhoeDR8'
   $ curl -H "Authorization: Bearer $token" http://51.68.230.193/restapi/beer/beer-type/
   HTTP/1.1 200 OK
   Content-Type: application/json
   Allow: GET, POST, HEAD, OPTIONS

.. code-block:: json

   {
      "count": 5,
      "next": null,
      "previous": null,
      "results": [
          {
              "id": 5,
              "name": "Rousse"
          },
          {
              "id": 4,
              "name": "Brune"
          },
          {
              "id": 3,
              "name": "Blonde"
          },
          {
              "id": 2,
              "name": "Blanche"
          },
          {
              "id": 1,
              "name": "IPA"
          }
      ]
   }
